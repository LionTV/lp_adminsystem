_menuPool = NativeUI.CreatePool()
local showTags = false
local seeTags = false
local staffTable = { 0 }
local TagDistance = 25
local distanceToCheck = 5.0
local numRetries = 5
local bool = false
local isFrozen;
local coordsVisible = false
local noclipActive = false
local index = 1
ESX = nil
Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)
_menuPool:RefreshIndex()
Citizen.CreateThread(function()
    while true do
        _menuPool:ProcessMenus()
        if IsControlJustReleased(1, 74) then
            TriggerServerEvent('lp_adminsystem:updatePlayer')
        end
        Citizen.Wait(1)
    end
end)

RegisterNetEvent('freeze:freezePlayer')
AddEventHandler('freeze:freezePlayer', function()
    isFrozen = not isFrozen
end)

Citizen.CreateThread(function()
    while true do
        FreezeEntityPosition(GetPlayerPed(-1), isFrozen)

        Citizen.Wait(0)
    end
end)

Citizen.CreateThread(function()
    while true do
        local sleepThread = 250

        if coordsVisible then
            sleepThread = 5

            local playerPed = PlayerPedId()
            local playerX, playerY, playerZ = table.unpack(GetEntityCoords(playerPed))
            local playerH = GetEntityHeading(playerPed)

            DrawGenericText(("~o~X~w~: %s ~o~Y~w~: %s ~o~Z~w~: %s ~o~H~w~: %s"):format(FormatCoord(playerX), FormatCoord(playerY), FormatCoord(playerZ), FormatCoord(playerH)))
        end

        Citizen.Wait(sleepThread)
    end
end)

Citizen.CreateThread(function()

    buttons = setupScaleform("instructional_buttons")

    currentSpeed = config.speeds[index].speed

    while true do
        Citizen.Wait(1)
        if noclipActive then
            DrawScaleformMovieFullscreen(buttons)

            local yoff = 0.0
            local zoff = 0.0

            if IsControlJustPressed(1, config.controls.changeSpeed) then
                if index ~= 8 then
                    index = index+1
                    currentSpeed = config.speeds[index].speed
                else
                    currentSpeed = config.speeds[1].speed
                    index = 1
                end
                setupScaleform("instructional_buttons")
            end

            DisableControls()

            if IsDisabledControlPressed(0, config.controls.goForward) then
                yoff = config.offsets.y
            end

            if IsDisabledControlPressed(0, config.controls.goBackward) then
                yoff = -config.offsets.y
            end

            if IsDisabledControlPressed(0, config.controls.turnLeft) then
                SetEntityHeading(noclipEntity, GetEntityHeading(noclipEntity)+config.offsets.h)
            end

            if IsDisabledControlPressed(0, config.controls.turnRight) then
                SetEntityHeading(noclipEntity, GetEntityHeading(noclipEntity)-config.offsets.h)
            end

            if IsDisabledControlPressed(0, config.controls.goUp) then
                zoff = config.offsets.z
            end

            if IsDisabledControlPressed(0, config.controls.goDown) then
                zoff = -config.offsets.z
            end

            local newPos = GetOffsetFromEntityInWorldCoords(noclipEntity, 0.0, yoff * (currentSpeed + 0.3), zoff * (currentSpeed + 0.3))
            local heading = GetEntityHeading(noclipEntity)
            SetEntityVelocity(noclipEntity, 0.0, 0.0, 0.0)
            SetEntityRotation(noclipEntity, 0.0, 0.0, 0.0, 0, false)
            SetEntityHeading(noclipEntity, heading)
            SetEntityCoordsNoOffset(noclipEntity, newPos.x, newPos.y, newPos.z, noclipActive, noclipActive, noclipActive)
        end
    end
end)

RegisterNetEvent("lp_adminsystem:openMenu")
AddEventHandler("lp_adminsystem:openMenu", function(players)
    mainMenu = NativeUI.CreateMenu('Trashlife', 'Admin-Menü')
    _menuPool:Add(mainMenu)
    --Erstes Submenu-----------------------------
    local submenu = _menuPool:AddSubMenu(mainMenu, "Support >>")
    local tptoway = NativeUI.CreateItem('Teleport to waypoint', '')
    local checkbox = NativeUI.CreateCheckboxItem("Show ID", bool, "")
    submenu:AddItem(tptoway)
    submenu:AddItem(checkbox)
    submenu.OnCheckboxChange = function (sender, item, checked_)
        if item == checkbox then
            bool = checked_
            if bool == true then
                showTags = true
                Notify('Die ID der anderen Spieler ist nun sichtbar!')
            else
                bool = false
                showTags = false
                Notify('Die ID der anderen Spieler ist nun nicht mehr sichtbar!')
            end
        end
    end
    submenu.OnItemSelect = function(sender, item, index)
        if item == tptoway then
            local waypoint = GetFirstBlipInfoId(8)
            if DoesBlipExist(waypoint) then
                local playerPed = PlayerPedId()
                local waypointCoords = GetBlipInfoIdCoord(waypoint)
                SetEntityCoords(playerPed, waypointCoords.x, waypointCoords.y, waypointCoords.z+35)
            end
        end
    end
    --Erstes Submenu-----------------------------

    --Zweites Submenu----------------------------
    local submenu2 = _menuPool:AddSubMenu(mainMenu, "Playeroptionen >>")
    local kickitem = NativeUI.CreateItem('Kick player', '')
    local sendmsg = NativeUI.CreateItem('Send message', '')
    local toggleNC = NativeUI.CreateItem('Noclip', '')
    local playerList = NativeUI.CreateListItem("Spieler", players ,0)
    local healplayer = NativeUI.CreateItem('Heal player', '')
    local revive = NativeUI.CreateItem('Revive player', '')
    local giveItem = NativeUI.CreateItem('Give Item', '')
    local giveMoney = NativeUI.CreateItem('Give Money', '')
    local platzhalter = NativeUI.CreateItem('----------------------------------------------------------', '')
    local platzhalter2 = NativeUI.CreateItem('----------------------------------------------------------', '')

    submenu2:AddItem(toggleNC)
    submenu2:AddItem(kickitem)
    submenu2:AddItem(sendmsg)
    submenu2:AddItem(playerList)
    submenu2:AddItem(platzhalter2)
    submenu2:AddItem(healplayer)
    submenu2:AddItem(revive)
    submenu2:AddItem(platzhalter)
    submenu2:AddItem(giveItem)
    submenu2:AddItem(giveMoney)
    submenu2.OnItemSelect = function(sender, item, index)
        if item == healplayer then
            AddTextEntry('eingabe', 'ID:')
            DisplayOnscreenKeyboard(1, "eingabe", "", "", "", "", "", 25)
            while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
                Citizen.Wait(100)
            end
            input = GetOnscreenKeyboardResult()
            if input ~= '' then
                TriggerServerEvent('lp_adminsystem:healplayer', input)
                Notify('Spieler geheilt.')
            end
        elseif item == toggleNC then
            noclipActive = not noclipActive
            if IsPedInAnyVehicle(PlayerPedId(), false) then
                noclipEntity = GetVehiclePedIsIn(PlayerPedId(), false)
            else
                noclipEntity = PlayerPedId()
            end

            SetEntityCollision(noclipEntity, not noclipActive, not noclipActive)
            FreezeEntityPosition(noclipEntity, noclipActive)
            SetEntityInvincible(noclipEntity, noclipActive)
            SetVehicleRadioEnabled(noclipEntity, not noclipActive) -- [[Stop radio from appearing when going upwards.]]
        elseif item == sendmsg then
            eingabe('ID: ', 25, 'sendmsg')
        elseif item == revive then
            TriggerEvent('esx_ambulancejob:revive')
        elseif item == giveMoney then
            eingabe("Eingabe: ", 25, 'lp_adminsystem:giveMoney')
        elseif item == giveItem then
            eingabe("Eingabe: ", 25, 'lp_adminsystem:giveItem')
        elseif item == kickitem then
            eingabe('ID: ', 25, 'kick')
        end
    end
    --Zweites Submenu----------------------------

    --Drittes Submenu----------------------------
    local submenu3 = _menuPool:AddSubMenu(mainMenu, "Serveroptionen >>")
    local startresource = NativeUI.CreateItem('Start script','')
    local restartscript = NativeUI.CreateItem('Restart script', '')
    local stopresource = NativeUI.CreateItem('Stop script', '')
    local serverad = NativeUI.CreateItem('Announce', '')
    local kick= NativeUI.CreateItem('Kick all', 'Kick all players')
    local coorditem = NativeUI.CreateItem('Show coords', '')
    local platzhalter3 = NativeUI.CreateItem('----------------------------------------------------------', '')
    submenu3:AddItem(startresource)
    submenu3:AddItem(restartscript)
    submenu3:AddItem(stopresource)
    submenu3:AddItem(platzhalter3)
    submenu3:AddItem(serverad)
    submenu3:AddItem(kick)
    submenu3:AddItem(coorditem)
    submenu3.OnItemSelect = function(sender, item, index)
        if item == serverad then
            eingabe('Eingabe: ', 50, 'lp_adminsystem:ann')
        elseif item == coorditem then
            ToggleCoords()
        elseif item == kick then
            eingabe('Grund: ', 50, 'kickall')
        elseif item == startresource then
            AddTextEntry('eingabe', 'Scriptname:')
            DisplayOnscreenKeyboard(1, "eingabe", "", "", "", "", "", 25)
            while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
                Citizen.Wait(100)
            end
            input = GetOnscreenKeyboardResult()
            if input ~= '' then
                TriggerServerEvent('lp_adminsystem:startRS', input)
            end
        elseif item == restartscript then
            AddTextEntry('eingabe', 'Scriptname:')
            DisplayOnscreenKeyboard(1, "eingabe", "", "", "", "", "", 25)
            while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
                Citizen.Wait(100)
            end
            input = GetOnscreenKeyboardResult()
            if input ~= '' then
                TriggerServerEvent('lp_adminsystem:restartRS', input)
            end
        elseif item == stopresource then
            AddTextEntry('eingabe', 'Scriptname:')
            DisplayOnscreenKeyboard(1, "eingabe", "", "", "", "", "", 25)
            while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
                Citizen.Wait(100)
            end
            input = GetOnscreenKeyboardResult()
            if input ~= '' then
                TriggerServerEvent('lp_adminsystem:stopRS', input)
            end
        end
    end
    --Drittes Submenu----------------------------

    --Viertes Submenu----------------------------
    local submenu4 = _menuPool:AddSubMenu(mainMenu, "Vehicleoptionen >>") 
    local spawnveh = NativeUI.CreateItem('Spawn Vehicle', '')
    local deleteveh = NativeUI.CreateItem('~r~Delete Vehicle', '')
    submenu4:AddItem(spawnveh)
    submenu4:AddItem(deleteveh)
    submenu4.OnItemSelect = function(sender, item, index)
        if item == spawnveh then
            eingabe('Eingabe: ', 25, 'spawncar')
        elseif item == deleteveh then
            TriggerEvent( "lp_adminsystem:deleteVehicle" )
        end
    end
    --Viertes Submenu----------------------------
    mainMenu:Visible(true)
    _menuPool:RefreshIndex()
    _menuPool:MouseEdgeEnabled(false)
end)

function eingabe(ueberschrift, maxlenght, event)
    AddTextEntry('eingabe', ueberschrift)
    DisplayOnscreenKeyboard(1, "eingabe", "", "", "", "", "", maxlenght)
    while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
        Citizen.Wait(100)
    end
    input = GetOnscreenKeyboardResult()
    if event == 'lp_adminsystem:giveMoney' then
        TriggerServerEvent('lp_adminsystem:giveMoney', input)
    elseif event == 'lp_adminsystem:giveItem' then
        TriggerServerEvent('lp_adminsystem:giveItem', input)
    elseif event == 'revive' then
        --TriggerEvent('esx_ambulancejob:revive', tonumber(input))
    elseif event == 'lp_adminsystem:ann' then
        if input ~= '' then
            TriggerServerEvent('lp_adminsystem:ann', input)
        end
    elseif event == 'spawncar' then
        spawnCar(input)
    elseif event == 'sendmsg' then
        AddTextEntry('eingabe', 'Text:')
        DisplayOnscreenKeyboard(1, "eingabe", "", "", "", "", "", 100)
        while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
            Citizen.Wait(100)
        end
        eing = GetOnscreenKeyboardResult()
        if eing ~= '' and input ~= '' then
            TriggerServerEvent('lp_adminsystem:adminPn', input, eing)
        end
    elseif event == 'kickall' then
        TriggerServerEvent('lp_adminsystem:kickall', input)
    elseif event == 'kick' then
        AddTextEntry('eingabe', 'Grund:')
        DisplayOnscreenKeyboard(1, "eingabe", "", "", "", "", "", 50)
        while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
            Citizen.Wait(100)
        end
        eing = GetOnscreenKeyboardResult()
        if eing ~= '' and input ~= '' then
            TriggerServerEvent('lp_adminsystem:kick', input, eing)
        end
    end
end

--functions

FormatCoord = function(coord)
    if coord == nil then
        return "unknown"
    end

    return tonumber(string.format("%.2f", coord))
end

ToggleCoords = function()
    coordsVisible = not coordsVisible
end

function DrawGenericText(text)
    SetTextColour(186, 186, 186, 255)
    SetTextFont(7)
    SetTextScale(0.378, 0.378)
    SetTextWrap(0.0, 1.0)
    SetTextCentre(false)
    SetTextDropshadow(0, 0, 0, 0, 255)
    SetTextEdge(1, 0, 0, 0, 205)
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(0.40, 0.00)
end

function Notify(text)
    SetNotificationTextEntry( "STRING")
    AddTextComponentString(text)
    DrawNotification(false, false)
end

function spawnCar(veh)
    local car = GetHashKey(veh)
    RequestModel(car)
    while not HasModelLoaded(car) do
        RequestModel(car)
        Citizen.Wait(50)
    end
    local x, y, z = table.unpack(GetEntityCoords(PlayerPedId(), false))
    local vehicle = CreateVehicle(car, x + 2, y + 2, z + 1, GetEntityHeading(PlayerPedId()), true, false)
    SetPedIntoVehicle(PlayerPedId(), vehicle, -1)
    SetEntityAsNoLongerNeeded(vehicle)
    SetModelAsNoLongerNeeded(vehicleName)
end

function ManageHeadLabels()
	for i = 0, 255 do
		if NetworkIsPlayerActive(i) then
			local iPed = GetPlayerPed(i)
			local lPed = PlayerPedId()
			if iPed ~= lPed then
				if DoesEntityExist(iPed) then
					distance = math.ceil(GetDistanceBetweenCoords(GetEntityCoords(lPed), GetEntityCoords(iPed)))
					if HasEntityClearLosToEntity(lPed, iPed, 17) or seeTags then
						if distance < TagDistance and showTags then
							if NetworkIsPlayerTalking(i) then
								headDisplayId = N_0xbfefe3321a3f5015(iPed, "", false, false, "", false )
								SetMpGamerTagAlpha(headDisplayId, 4, 225)							
								SetMpGamerTagVisibility(headDisplayId, 4, true)
							else
								headDisplayId = N_0xbfefe3321a3f5015(iPed, "", false, false, "", false )
								if has_value(staffTable,GetPlayerServerId(i)) then 
									SetMpGamerTagName(headDisplayId,DisplayStaffTag.." ID: "..GetPlayerServerId(i))
									SetMpGamerTagColour(headDisplayId, 0, 6)
								else
									SetMpGamerTagColour(headDisplayId, 0, 66)
									SetMpGamerTagName(headDisplayId," ID: "..GetPlayerServerId(i))
								end
								SetMpGamerTagVisibility(headDisplayId, 4, false)
								SetMpGamerTagVisibility(headDisplayId, 0, true)
							end
						else
							headDisplayId = N_0xbfefe3321a3f5015(iPed, "", false, false, "", false )
							SetMpGamerTagName(headDisplayId,GetPlayerServerId(i))
							SetMpGamerTagVisibility(headDisplayId, 0, false)
							SetMpGamerTagVisibility(headDisplayId, 7, false)
						end
					else
						headDisplayId = N_0xbfefe3321a3f5015(iPed, "", false, false, "", false )
						SetMpGamerTagName(headDisplayId,GetPlayerServerId(i))
						SetMpGamerTagVisibility(headDisplayId, 0, false)
						SetMpGamerTagVisibility(headDisplayId, 7, false)
					end
				end
			end
		end
	end
end

function has_value (tab, val)
    for i, v in ipairs (tab) do
        if (v == val) then
            return true
        end
    end
    return false
end

Citizen.CreateThread(function()
	while true do
		ManageHeadLabels()
		Citizen.Wait(1)
	end
end)

RegisterNetEvent("lp_adminsystem:healpl")
AddEventHandler("lp_adminsystem:healpl", function()
    SetEntityHealth(PlayerPedId(), 200)
    TriggerEvent('esx_status:set', 'hunger', 1000000)
    TriggerEvent('esx_status:set', 'thirst', 1000000)
end)

RegisterNetEvent("lp_adminsystem:deleteVehicle")
AddEventHandler("lp_adminsystem:deleteVehicle", function()
    local ped = GetPlayerPed( -1 )
    if (DoesEntityExist(ped) and not IsEntityDead(ped)) then 
        local pos = GetEntityCoords(ped)
        if (IsPedSittingInAnyVehicle(ped)) then 
            local vehicle = GetVehiclePedIsIn(ped, false)

            if (GetPedInVehicleSeat(vehicle, -1) == ped) then 
                DeleteGivenVehicle(vehicle, numRetries)
            end 
        else
            local inFrontOfPlayer = GetOffsetFromEntityInWorldCoords(ped, 0.0, distanceToCheck, 0.0)
            local vehicle = GetVehicleInDirection(ped, pos, inFrontOfPlayer)
            if (DoesEntityExist(vehicle)) then 
                DeleteGivenVehicle(vehicle, numRetries)
            end 
        end 
    end 
end)

function DeleteGivenVehicle(veh, timeoutMax)
    local timeout = 0 
    SetEntityAsMissionEntity(veh, true, true)
    DeleteVehicle(veh)
    if (DoesEntityExist(veh)) then
        while (DoesEntityExist(veh) and timeout < timeoutMax) do 
            DeleteVehicle(veh)
            timeout = timeout + 1 
            Citizen.Wait(200)
        end 
    end 
end 

function GetVehicleInDirection(entFrom, coordFrom, coordTo)
	local rayHandle = StartShapeTestCapsule(coordFrom.x, coordFrom.y, coordFrom.z, coordTo.x, coordTo.y, coordTo.z, 5.0, 10, entFrom, 7)
    local _, _, _, _, vehicle = GetShapeTestResult(rayHandle)
    if (IsEntityAVehicle(vehicle)) then 
        return vehicle
    end 
end

function ButtonMessage(text)
    BeginTextCommandScaleformString("STRING")
    AddTextComponentScaleform(text)
    EndTextCommandScaleformString()
end

function Button(ControlButton)
    N_0xe83a3e3557a56640(ControlButton)
end

function setupScaleform(scaleform)

    local scaleform = RequestScaleformMovie(scaleform)

    while not HasScaleformMovieLoaded(scaleform) do
        Citizen.Wait(1)
    end

    PushScaleformMovieFunction(scaleform, "CLEAR_ALL")
    PopScaleformMovieFunctionVoid()

    PushScaleformMovieFunction(scaleform, "SET_CLEAR_SPACE")
    PushScaleformMovieFunctionParameterInt(200)
    PopScaleformMovieFunctionVoid()

    PushScaleformMovieFunction(scaleform, "SET_DATA_SLOT")
    PushScaleformMovieFunctionParameterInt(5)
    Button(GetControlInstructionalButton(2, config.controls.openKey, true))
    ButtonMessage("Disable Noclip")
    PopScaleformMovieFunctionVoid()

    PushScaleformMovieFunction(scaleform, "SET_DATA_SLOT")
    PushScaleformMovieFunctionParameterInt(4)
    Button(GetControlInstructionalButton(2, config.controls.goUp, true))
    ButtonMessage("Go Up")
    PopScaleformMovieFunctionVoid()

    PushScaleformMovieFunction(scaleform, "SET_DATA_SLOT")
    PushScaleformMovieFunctionParameterInt(3)
    Button(GetControlInstructionalButton(2, config.controls.goDown, true))
    ButtonMessage("Go Down")
    PopScaleformMovieFunctionVoid()

    PushScaleformMovieFunction(scaleform, "SET_DATA_SLOT")
    PushScaleformMovieFunctionParameterInt(2)
    Button(GetControlInstructionalButton(1, config.controls.turnRight, true))
    Button(GetControlInstructionalButton(1, config.controls.turnLeft, true))
    ButtonMessage("Turn Left/Right")
    PopScaleformMovieFunctionVoid()

    PushScaleformMovieFunction(scaleform, "SET_DATA_SLOT")
    PushScaleformMovieFunctionParameterInt(1)
    Button(GetControlInstructionalButton(1, config.controls.goBackward, true))
    Button(GetControlInstructionalButton(1, config.controls.goForward, true))
    ButtonMessage("Go Forwards/Backwards")
    PopScaleformMovieFunctionVoid()

    PushScaleformMovieFunction(scaleform, "SET_DATA_SLOT")
    PushScaleformMovieFunctionParameterInt(0)
    Button(GetControlInstructionalButton(2, config.controls.changeSpeed, true))
    ButtonMessage("Change Speed ("..config.speeds[index].label..")")
    PopScaleformMovieFunctionVoid()

    PushScaleformMovieFunction(scaleform, "DRAW_INSTRUCTIONAL_BUTTONS")
    PopScaleformMovieFunctionVoid()

    PushScaleformMovieFunction(scaleform, "SET_BACKGROUND_COLOUR")
    PushScaleformMovieFunctionParameterInt(config.bgR)
    PushScaleformMovieFunctionParameterInt(config.bgG)
    PushScaleformMovieFunctionParameterInt(config.bgB)
    PushScaleformMovieFunctionParameterInt(config.bgA)
    PopScaleformMovieFunctionVoid()

    return scaleform
end

function DisableControls()
    DisableControlAction(0, 30, true)
    DisableControlAction(0, 31, true)
    DisableControlAction(0, 32, true)
    DisableControlAction(0, 33, true)
    DisableControlAction(0, 34, true)
    DisableControlAction(0, 35, true)
    DisableControlAction(0, 266, true)
    DisableControlAction(0, 267, true)
    DisableControlAction(0, 268, true)
    DisableControlAction(0, 269, true)
    DisableControlAction(0, 44, true)
    DisableControlAction(0, 20, true)
end