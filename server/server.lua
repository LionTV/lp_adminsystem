ESX                            = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('lp_adminsystem:updatePlayer')
AddEventHandler('lp_adminsystem:updatePlayer', function()
    local player = {
        "Test"
    }
    local players = GetPlayers()
    for _, i in ipairs(players) do
        local pnames = GetPlayerName(i)
        table.insert(player, pnames)
    end
    TriggerClientEvent('lp_adminsystem:openMenu', source, player)
end)

RegisterServerEvent('lp_adminsystem:giveMoney') 
AddEventHandler('lp_adminsystem:giveMoney', function(money)
    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    xPlayer.addMoney(money)
    TriggerClientEvent('lp_notify', source , "Announce", "Dir wurde Geld gegeben", 10)
end)

RegisterServerEvent('lp_adminsystem:adminPn')
AddEventHandler('lp_adminsystem:adminPn', function(id, text)
    TriggerClientEvent('esx:showAdvancedNotification', id, 'Meldung des Serverteams', 'Administrator', text,'CHAR_DEFAULT', 8)
end)

RegisterServerEvent('lp_adminsystem:ann') 
AddEventHandler('lp_adminsystem:ann', function(text) 
    TriggerClientEvent('lp_notify', -1, "Announce", text, 10)
end)

RegisterServerEvent('lp_adminsystem:kickall')
AddEventHandler('lp_adminsystem:kickall', function(reason)
    local xPlayers = ESX.GetPlayers()
    for i=1, #xPlayers, 1 do
        local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
        print(xPlayer)
        xPlayer.kick(reason)
    end
end)

RegisterServerEvent('lp_adminsystem:kick')
AddEventHandler('lp_adminsystem:kick', function(id, reason)
    DropPlayer(id, reason)
end)

RegisterServerEvent('lp_adminsystem:healplayer')
AddEventHandler('lp_adminsystem:healplayer', function(id)
    TriggerClientEvent('lp_adminsystem:healpl', id)
end)

RegisterServerEvent('lp_adminsystem:startRS')
AddEventHandler('lp_adminsystem:startRS', function(script)
    StartResource(script)
end)
RegisterServerEvent('lp_adminsystem:restartRS')
AddEventHandler('lp_adminsystem:restartRS', function(script)
    StopResource(script)
    StartResource(script)
end)
RegisterServerEvent('lp_adminsystem:stopRS')
AddEventHandler('lp_adminsystem:stopRS', function(script)
    StopResource(script)
end)

TriggerClientEvent('freeze:freezePlayer', tonumber(id))