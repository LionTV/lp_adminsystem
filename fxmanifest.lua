fx_version 'bodacious'
author 'Leon.'
game 'gta5'

client_scripts {
    '@NativeUI/NativeUI.lua',
    'config.lua',
    'client/client.lua'
}

server_scripts {
    'config.lua',
    'server/server.lua'
}